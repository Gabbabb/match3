QUIT = "q"
local cmd_keys = {"name", "x", "y", "dir"}

local command = {}
--[[
cmd
cmd.name
cmd.dir
cmd.pos.x
cmd.pos.y
--]]

function command.end_game_cmd(cmd)
  return cmd.name == "q"
end

-- создание объекта cmd из строки вида "m 5 3 r"
local function _parse(raw_cmd_str)
  local cmd = {}
  local index = 1
  -- [^%s]+ означает "последовательности из любых символов, не содержащие пробелов" (Split по пробелу)
  for cmd_symbs in string.gmatch(raw_cmd_str, "[^%s]+") do
    cmd[cmd_keys[index]] = cmd_symbs
    index = index + 1
  end
  cmd.pos = {}
  if cmd.x == nil or cmd.y == nil then
    return cmd
  end  
  cmd.pos.x = tonumber(cmd.x) + 1
  cmd.x = nil
  cmd.pos.y = tonumber(cmd.y) + 1
  cmd.y = nil
  return cmd
end

-- ввод команды вида "m 5 3 r"
-- проверка входных данных сейчас отсутствует
function command.from_console()
  local cmd = {}
  local raw_cmd_str = io.read()
  cmd = _parse(raw_cmd_str)
  return cmd
end

return command