local crystall = {}

crystall.colors = {"A","B","C","D","E","F"}

function crystall.create(color)
  local crystall = {} 
  crystall.color = color
  return crystall
end

function crystall.create_random()
  random_color = crystall.colors[math.random(1,6)]
  return crystall.create(random_color)
end

function crystall.create_random(not_forbidden_colors)
  random_color = not_forbidden_colors[math.random(1, #not_forbidden_colors)]
  return crystall.create(random_color)
end


-- возвращает массив, заполненный цветами, которые не вхолят в forbidden_colors
function crystall.forbid_colors(forbidden_colors)
  local not_forbidden_colors = {}
  local color_forbidden = false
  -- лучше обходить forbidden_colors во внешнем цикле
  for index = 1, #crystall.colors do
    crystall_color = crystall.colors[index]
    for jindex = 1, #forbidden_colors do
      color_forbidden = crystall_color  == forbidden_colors[jindex]
      if color_forbidden then        
        break
      end
    end
    if not color_forbidden then
      not_forbidden_colors[#not_forbidden_colors + 1] = crystall.colors[index]
    end
  end
  return not_forbidden_colors
end

return crystall