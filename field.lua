local crystall = require "crystall"
-- field[x, y]
local field = {}
local size_x = 10
local size_y = 10

-- запретить цвет, если при добавлении кристалла этого цвета получится тройка
local function forbid_colors_for(field, index, jindex)
  local forbidden_colors = {}
  if jindex > 2 and field[index][jindex-1].color == field[index][jindex-2].color then
    table.insert(forbidden_colors, field[index][jindex-1].color)
  end
  if index > 2 and field[index - 1][jindex].color == field[index - 2][jindex].color then
    table.insert(forbidden_colors, field[index-1][jindex].color)
  end
  --print(table.concat(forbidden_colors, ", "))
  return forbidden_colors
end

function field.gen_random()
local field = {}
field.size_x = size_x
field.size_y = size_y
-- для того, чтобы при генерации поля не создавались 3-ки, выбираем такой цвет для нового кристалла, чтобы он не совпадал с цветом любого соседнего
local not_forbidden_colors = crystall.colors
local forbidden_colors = {}
for index = 1, field.size_x
do  
  field[index] = {}  
  for jindex = 1, field.size_y
  do
    forbidden_colors = forbid_colors_for(field, index, jindex)   
    not_forbidden_colors = crystall.forbid_colors(forbidden_colors)
    rand_crystall = crystall.create_random(not_forbidden_colors)    
    field[index][jindex] = rand_crystall
  end
end
  return field
end

function field.gen_unplayable()
  local field = {}
  field.size_x = size_x
  field.size_y = size_y
  -- для того, чтобы при генерации поля не создавались 3-ки, выбираем такой цвет для нового кристалла, чтобы он не совпадал с цветом любого соседнего
  local not_forbidden_colors = crystall.colors
  local forbidden_colors = {}
  for index = 1, field.size_x
  do  
    field[index] = {}  
    for jindex = 1, field.size_y
    do
      forbidden_colors = {}
      if index > 1 then
        table.insert(forbidden_colors, field[index - 1][jindex].color)
      end
      if jindex > 1 then
        table.insert(forbidden_colors, field[index][jindex - 1].color)
      end
      not_forbidden_colors = crystall.forbid_colors(forbidden_colors)
      rand_crystall = crystall.create_random(not_forbidden_colors)    
      field[index][jindex] = rand_crystall
    end    
  end
  return field
  end


return field