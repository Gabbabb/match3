UP = "u"
DOWN = "d"
RIGHT = "r"
LEFT = "l"
local field = require "field"
local crystall = require "crystall"
local visual = require "visual"

-- Модель с основной бизнес-логикой
local game = {}

local _field = {}
-- status enum
local SmthChanged, FillingHoles, DroppingNewCrystalls, FieldInBalance, WaitingForInput = 1,2,3,4,5
local _status
local pos_to_check = {}
local pos_to_delete = {}

-- PRIVATE
-- отмечает count элементов на удаление, начинает с _field[x][y] и двигается вверх
local function mark_for_delete(x, y, count)
  for index = count, _field.size_y - count do
    pos = {}
    pos.x = x
    pos.y = index
    append(pos_to_delete, pos)
  end
end

-- создание единичного вектора
local function create_dir(x, y)

  local dir = {}
  dir.x = x
  dir.y = y
  return dir
end

local function concat(array1, array2)
  local new_table = {}
  for _, val in pairs(array1) do
    table.insert(new_table, val)
  end
  for _, val in pairs(array2) do
    table.insert(new_table, val)
  end
  return new_table  
end

-- x, y цель скана; count 
local function scan(root, dir)
  local sequence = {}
  local color = _field[root.x][root.y].color    
  index = 1
  local pos = {}
  pos.x = root.x + dir.x * index
  pos.y = root.y + dir.y * index
  while pos.x >= 1 and pos.x <= 10 and pos.y >= 1 and pos.y <= 10 do 
    if _field[pos.x][pos.y].color == color then
      table.insert(sequence, pos)
    else
      return sequence
    end
    index = index + 1
    pos = {}
    pos.x = root.x + dir.x * index
    pos.y = root.y + dir.y * index
  end
  return sequence
end
local function scan_neighbors(pos)
  local left = scan(pos, create_dir(-1, 0))
  local right = scan(pos, create_dir(1, 0))
  local horizontal = concat(left, right)
  
  local up = scan(pos, create_dir(0, 1))
  local down = scan(pos, create_dir(0, -1))
  local vertical = concat(up, down)
  
  return horizontal, vertical
end
  
local function change_pos(from, to)
  local new_pos = {}
  new_pos.x = from.x
  new_pos.y = from.y
  if to == UP then
    if new_pos.y > 1 then
      new_pos.y = new_pos.y - 1
    end
    return new_pos
  end
  if to == DOWN then
    if new_pos.y < _field.size_y then
      new_pos.y = new_pos.y + 1
    end
    return new_pos
  end
  if to == LEFT then
    if new_pos.x > 1 then
      new_pos.x = new_pos.x - 1
    end
    return new_pos
  end
  if to == RIGHT then
    if new_pos.x < _field.size_x then
      new_pos.x = new_pos.x + 1
    end
    return new_pos
  end
  return new_pos
end

local function check_neighbors(pos)
  h, v = scan_neighbors(pos)
  return #h >= 2 or #v >= 2
end
local function swap(field, from, new_pos)
  field[from.x][from.y], field[new_pos.x][new_pos.y] =  field[new_pos.x][new_pos.y], field[from.x][from.y]
end
local function check_matches()
  pos_to_delete = {}
  for _, pos in pairs(pos_to_check) do
    local left = scan(pos, create_dir(-1, 0))
    local right = scan(pos, create_dir(1, 0))
    local horizontal = concat(left, right)
    
    local up = scan(pos, create_dir(0, 1))
    local down = scan(pos, create_dir(0, -1))
    local vertical = concat(up, down)
    
    if #horizontal >=2 then
      pos_to_delete = concat(pos_to_delete, horizontal)
    end
        
    if #vertical >= 2 then
      pos_to_delete = concat(pos_to_delete, vertical)
    end
    
    if #horizontal >= 2 or #vertical >=2 then
      table.insert(pos_to_delete, pos)
    end
  end
  return #pos_to_delete > 0
end


local function destroy_matches()
  local y_arr = {}
  while #pos_to_delete > 0 do    
    local pos = table.remove(pos_to_delete, #pos_to_delete)
    if y_arr[pos.x] == nil then
      y_arr[pos.x] = 10
    end
    _field[pos.x][pos.y] = nil
    if y_arr[pos.x] > pos.y then
      y_arr[pos.x] = pos.y
    end    
  end
  
  pos_to_check = {}
  for x, _ in pairs(y_arr) do
    for y = 1, 10 do      
      table.insert(pos_to_check, create_dir(x, y))
    end
  end
end

local function fill_holes()
  local swap_count = 0
  for index = 1, 10 do
    for jindex = 10, 2, -1 do
      if _field[index][jindex] == nil and _field[index][jindex - 1] ~= nil then
        -- swap
        _field[index][jindex], _field[index][jindex - 1] = _field[index][jindex - 1], _field[index][jindex]
        swap_count = swap_count + 1
      end
    end
  end
  -- возвращает true, если что-то сдвинулось
  return swap_count > 0
end

local function drop_new_crystalls()
  local dropped = false
  for index = 1,10 do
    if _field[index][1] == nil then
      _field[index][1] = crystall.create_random(crystall.colors)
      dropped = true
    end
  end
  return dropped
end

local function turn_exist()
  local pos1, pos2 = {}
  local exist = false
  for index = 1, 10 do
    for jindex = 1, 10 do
      -- можно ли сдвинуть направо
      pos1 = create_dir(index, jindex)
      if index < 10 then
        pos2 = create_dir(index + 1, jindex)
        swap(_field, pos1, pos2)
        exist = exist or check_neighbors(pos1) or check_neighbors(pos2)
        swap(_field, pos1, pos2)
        if exist then
          return exist
        end
      end
      -- можно ли сдвинуть вниз
      if jindex < 10 then
        pos2 = create_dir(index, jindex + 1)
        swap(_field, pos1, pos2)
        exist = exist or check_neighbors(pos1) or check_neighbors(pos2)
        swap(_field, pos1, pos2)
        if exist then
          return exist
        end
      end      
      end      
  return exist
end
end

-- PUBLIC
-- game.tick() реализует дерево поведения игры через конструкции if и изменение состояния, хранящегося в _status
-- возвращает true если что-то происходит, false если ничего не проиcходит и рисовать нечего
function game.tick()
  if _status == WaitingForInput then
    return false
  end
  if _status == SmthChanged then
    if not check_matches() then
      _status = FieldInBalance
      return true
    end
    destroy_matches()
    _status = FillingHoles
    return true
  end 
  
  if _status == FillingHoles then    
    if not fill_holes() then
      _status = DroppingNewCrystalls    
    end
    return true
  end
  
  if _status == DroppingNewCrystalls then
    if drop_new_crystalls() then
      _status = FillingHoles
    else
      _status = SmthChanged      
    end
    return true
  end
  
  if _status == FieldInBalance then
    while not turn_exist() do
      game.mix()
    end
      _status = WaitingForInput
    return true
  end
      
  return false
end
function game.move_crystall(from, to)
  if from.x < 0 or from.x > _field.size_x or from.y < 0 or from.y > _field.size_y then
    return
  end
  _status = SmthChanged
  print(string.format("Moving cryst on x:%i y:%i %s", from.x - 1, from.y - 1, to))
  local new_pos = change_pos(from, to)
  _field[from.x][from.y], _field[new_pos.x][new_pos.y] =  _field[new_pos.x][new_pos.y], _field[from.x][from.y]
  
  pos_to_check = {}
  if not (check_neighbors(from) or check_neighbors(new_pos)) then
    _status = FieldInBalance
    _field[from.x][from.y], _field[new_pos.x][new_pos.y] =  _field[new_pos.x][new_pos.y], _field[from.x][from.y]
    return
  end
  table.insert(pos_to_check, from)
  table.insert(pos_to_check, new_pos)
end

function game.init()
  _field = field.gen_random()
  --_field = field.gen_unplayable()
  -- надо, на всякий случай, "протикать" поле, если было создано неиграбельное поле, game.tick разберётся
  _status = FieldInBalance
  game.tick()
end

function game.dump()
  visual.dump(_field)
end

function game.mix()
  local pos1, pos2, swapped
  for index = 10, 1, -1 do
    for jindex = 10, 1, -1 do
      swapped = false
      while not swapped do
        pos1 = create_dir(math.random(1, index), math.random(1, jindex))
        pos2 = create_dir(index, jindex)
        swap(_field, pos1, pos2)
        if check_neighbors(pos1) or check_neighbors(pos2) then
          swap(_field, pos1, pos2)
        else
          --print(string.format("from x=%i y=%i to x=%i y=%i", pos1.x-1,pos1.y-1,pos2.x-1,pos2.y-1))
          --game.dump()
          swapped = true
        end
      end
    end
  end
end

return game
