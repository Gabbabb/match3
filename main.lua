local game = require "game"
local command = require "command"
-- инициализация
--game.debug = true
game.init()
game.dump()
-- main loop
local cmd = command.from_console()
while not command.end_game_cmd(cmd) do
  game.move_crystall(cmd.pos, cmd.dir)
  while game.tick() do
    game.dump()
  end
  cmd = command.from_console()
end