Основной игровой цикл:
main.lua

Бизнес-логика:
game.lua

Интерфейс пользователя и реализация:
command.lua
visual.lua

Поле и кристаллы:
field.lua
crystall.lua

В разработке использовал ZeroBrane Studio. Работал на Windows.

Лучшее, что придумал для очистки консоли:

if not os.execute("clear") then
  os.execute("cls")
end
print

На Windows работает некорректно.
Очистка экрана сейчас отсутствует.