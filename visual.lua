visual = {}

local clock = os.clock
function sleep(n)  -- seconds
  local t0 = clock()
  while clock() - t0 <= n do end
end

function visual.dump(field)
  sleep(0.5)
  print()
  print("  0 1 2 3 4 5 6 7 8 9")
  for index = 1, 10 do
    local out = tostring (index - 1)
    for jindex = 1, 10 do
      local str = ""
      if field[jindex][index] ~= nil then
        str = field[jindex][index].color
      else
        str = " "        
      end 
      out = out.." "..str
    end
    print(out)
  end
end

return visual